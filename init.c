
#include "bugbrain.h"

void initAdc(void)
{
    AD1CON1bits.ON = 0;

	AD1CHSbits.CH0NA = 0;
	AD1CHSbits.CH0SA = 0;

	AD1CON1bits.FORM = 0;

	AD1CON1bits.SSRC = 0b000;
	AD1CON1bits.ASAM = 0;
	AD1CSSL = 0;

	AD1CON2bits.VCFG = 0;
	AD1CON2bits.CSCNA = 0;
	AD1CON2bits.ALTS = 0;

	AD1CON3bits.ADRC = 1;
	AD1CON3bits.ADCS = 0b00111111;
	AD1CON3bits.SAMC = 0b11111;
}

void init_timer1(void)
{
    // 4MHz with Configbits (FRC and DIV_2)
    OSCTUNbits.TUN = 0;
    T1CONbits.ON = 0;
    PR1 = 0x0FFF;
    T1CONbits.TCS = 0; //Set to internal clock
    T1CONbits.TCKPS = 0;
    TMR1 = 0;
}

void init_timer2(void)
{
    OSCTUNbits.TUN = 0;
    T2CONbits.ON = 0;
    PR2 = 0xFFFF; // 4MHz with Configbits (FRC and DIV_2)
    T2CONbits.TCS = 0; //Set to internal clock
    T2CONbits.TCKPS = 0;
    TMR2 = 0;
}

void init_timer3(void)
{
    OSCTUNbits.TUN = 0;
    T3CONbits.ON = 0;
    PR3 = 0xFFFF; // 4MHz with Configbits (FRC and DIV_2)
    T3CONbits.TCS = 0; //Set to internal clock
    T3CONbits.TCKPS = 0b111;
    TMR3 = 0;
}

void wait1(s32 cnt)
{
    u32 i = 0;
    for (i = 0; i < cnt; i++)
    {
        T1CONbits.ON = 1;
        while (TMR1 != PR1);
        if(TMR1 = PR1)
            T1CONbits.ON = 0;
    }
}

void wait(s32 cnt)
{
	while (cnt--);
}

void wait2(s32 cnt)
{
    u32 i = 0;
    for (i = 0; i < cnt; i++)
    {
        T2CONbits.ON = 1;
        while (TMR2 != PR2);
        if(TMR2 = PR2)
            T2CONbits.ON = 0;
    }
}

void wait3(s32 cnt)
{
    u32 i = 0;
    for (i = 0; i < cnt; i++)
    {
        T3CONbits.ON = 1;
        while (TMR3 != PR3);
        if(TMR3 = PR3)
            T3CONbits.ON = 0;
    }
}

void init(void)
{
    // Timers
    init_timer1();
    init_timer2();

	//LCD GPIO
    PMCONbits.ON = 0;
    PMAENbits.PTEN8 = 0;
    PMAENbits.PTEN9 = 0;
	PMAENbits.PTEN15 = 0;

    // Set LCD pins to output
    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC4 = 0;
    TRISCbits.TRISC3 = 0;
    TRISAbits.TRISA9 = 0;
    TRISAbits.TRISA4 = 0;
    TRISBbits.TRISB4 = 0;
    TRISAbits.TRISA8 = 0;

    // LCD port latches
    RS = 0;
    RW = 0;
    ENABLE  = 0;
    DB4 = 0;
    DB5 = 0;
    DB6 = 0;
    DB7 = 0;

	//LED RGB
	TRISAbits.TRISA10 = 0;
	TRISAbits.TRISA7 = 0;
	TRISBbits.TRISB14 = 0;

	RED_SENS = 0;
	GREEN_SENS = 0;
	BLUE_SENS = 0;

    //AD1PCFGbits.PCFG0 = 0;
    TRISAbits.TRISA0 = 1;
    TRISAbits.TRISA1 = 1;
}

void initSPI()
{
	ANSELBbits.ANSB15 = 0;
	ANSELBbits.ANSB13 =0;

        CM3CONbits.ON = 0;
        CTMUCONbits.ON = 0;
        PMCONbits.ON = 0;

	TRISBbits.TRISB15 = 0;
	TRISBbits.TRISB13 = 1;
	TRISAbits.TRISA2 = 0;
	TRISAbits.TRISA3 = 0;

        CFGCONbits.IOLOCK = 0;
        CFGCONbits.PMDLOCK = 0;
        SDI2R = 3;
        CFGCONbits.IOLOCK = 1;
        CFGCONbits.PMDLOCK = 1;

        CFGCONbits.IOLOCK = 0;
        CFGCONbits.PMDLOCK = 0;

        RPA2R = 4;
        CFGCONbits.IOLOCK = 1;
        CFGCONbits.PMDLOCK = 1;


	LATAbits.LATA3 = 1;

	SPI2CONbits.ON = 0;
	SPI2BUF = 0;
	SPI2BRG = 255;
	SPI2STATbits.SPIROV = 0;

	SPI2CONbits.FRMEN = 0;
	SPI2CONbits.SIDL = 0;
	SPI2CONbits.DISSDO = 0;

	SPI2CONbits.MODE16 = 0;
	SPI2CONbits.MODE32 = 0;
	SPI2CONbits.CKP = 0;
	SPI2CONbits.CKE = 1;
	SPI2CONbits.SSEN = 0;
	SPI2CONbits.MSTEN = 1;
	SPI2CONbits.SMP = 0;

	SPI2CONbits.ON = 1;
}