
#include "bugbrain.h"

void send(u8 rs, u8 rw, u8 data)
{
    RS = rs;
    RW = rw;

    wait1(1);
    ENABLE = 1;

    DB4 = 0;
    DB5 = 0;
    DB6 = 0;
    DB7 = 0;

    DB4 = (data >> 4) & 1;
    DB5 = (data >> 5) & 1;
    DB6 = (data >> 6) & 1;
    DB7 = (data >> 7) & 1;

    wait1(1);
    ENABLE = 0;
}

void lcd_init(void)
{
    wait1(1);
    send(0,0,0b00110000);
    wait1(1);
    send(0,0,0b00110000);
    wait1(1);
    send(0,0,0b00110000);
    wait1(1);
    send(0,0,0b00100000);

    wait1(1);
    send(0,0,0b00100000);
    send(0,0,0b10000000);
    wait1(1);
    send(0,0,0b00000000);
    send(0,0,0b10000000);
    wait1(1);
    send(0,0,0b00000000);
    send(0,0,0b00010000);
    wait1(1);
    send(0,0,0b00000000);
    send(0,0,0b01100000);

    wait1(1);
    send(0,0,0b00100000);
    send(0,0,0b10000000);

    wait1(1);
    send(0,0,0b00000000);
    send(0,0,0b11110000);

    wait1(1);
    send(0,0,0b00000000);
    send(0,0,0b01100000);
}

void send_data(u8 rs, u8 rw, u8 data)
{
    send(rs, rw, data);
    send(rs, rw, data << 4);
    //wait(1);
}

void putlcd(s8 *str)
{
    while(*str)
    {
        //wait(1);
        send_data(1,0,*str);
        str++;
    }
}

void clear(void)
{
	send_data(0,0, 0b00000001);
}

void move_left(u8 nb)
{
	u8 i;

	while (i < nb)
	{
		send_data(0,0,0b00010000);
		i++;
	}
}

void move_right(u8 nb)
{
	u8 i;

	while (i < nb)
	{
		send_data(0,0,0b00010100);
		i++;
	}
}