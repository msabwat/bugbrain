/* 
 * File:   bugbrain.h
 * Author: bocal
 *
 * Created on August 20, 2018, 12:14 AM
 */
#ifndef BUGBRAIN_H
#define	BUGBRAIN_H

#include <p32xxxx.h>

#ifdef	__cplusplus
extern "C" {
#endif


/* UTILS */
#define MIN(a,b) (a<b)?a:b
/* TYPES */
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef signed char s8;
typedef signed short s16;
typedef signed long s32;
// Commands
typedef struct command
{
    u8 color;
    u8 ins;
}           s_cmd;
//Bug brain Instruction Set
typedef struct isSet
{
    s_cmd main[20];
    s_cmd f1[20];
    s_cmd f2[20];
    s_cmd f3[20];
}           s_insSet;

/* RGB SENSOR LED ENABLE */
#define RED_SENS		LATAbits.LATA10
#define GREEN_SENS		LATAbits.LATA7
#define BLUE_SENS		LATBbits.LATB14

// References to compare to in ADC convertion
#define RED_REF			660
#define GREEN_REF		570
#define BLUE_REF		286

/* PORT NAMES */
#define RS  LATCbits.LATC5
#define RW  LATCbits.LATC4
#define ENABLE   LATCbits.LATC3
    
#define DB4 LATAbits.LATA9
#define DB5 LATAbits.LATA4
#define DB6 LATBbits.LATB4
#define DB7 LATAbits.LATA8

/* PROTOTYPES */

// INIT
void init_timer1(void);
void init_timer2(void);
void wait(s32 cnt);
void wait2(s32 cnt);
void init(void);
void initAdc(void);
void init_inst_set(void);

// LCD
void send(u8 rs, u8 rw, u8 data);
void lcd_init(void);
void send_data(u8 rs, u8 rw, u8 data);
void putlcd(s8 *str);
void clear(void);
void move_left(u8 nb);
void move_right(u8 nb);


// Stepper Motors
void FullAhead(u32 tour);
void TurnLeft(u32 tour);
void TurnRight(u32 tour);

//utils
u32 readAN(u8 channel);
void handle_key(u32 key_buf);

void run_motor(u8 ins);
u8 check_color(u8 color);
void	print_charf1(u8 pos);
void	print_charf2(u8 pos);
void	print_charf3(u8 pos);
void	print_charup(u8 pos);
void	print_charlft(u8 pos);
void	print_charrgt(u8 pos);

void	print_main(void);
void	print_f1(void);
void	print_f2(void);
void	print_f3(void);

u8 write2SPI(u8 i);
void write_enable(void);
u8 read_status(u8 dummy);
void write_eeprom(u8 data, u32 address);
u8 read_eeprom(u32 address);


/* ANALOG VALUES */
#define LFT	897
#define UP      871
#define RGT	807
#define MODE    721

#define RED     683
#define GREEN   636
#define BLUE    547
#define SAVE    417

#define F1	407
#define F2	356
#define F3	273
#define DEL	190

#define MV_LFT  178
#define RUN     153
#define MV_RGT  116
#define RESET   70


/* Instruction Set Macros */
#define I_F1	30
#define I_F2	40
#define I_F3	50

#define I_UP	80
#define I_RGT	60
#define I_LFT	100



#ifdef	__cplusplus
}
#endif

#endif	/* BUGBRAIN_H */

