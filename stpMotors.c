
#include "bugbrain.h"

void marcheDroite(u32 tour)
{
	while (tour)
	{
		u32 engine[8];
                u32 engineB[8];
                u8 i;

		engine[0] = 3;
		engine[1] = 3;
		engine[2] = 6;
		engine[3] = 6;
		engine[4] = 12;
		engine[5] = 12;
		engine[6] = 9;
		engine[7] = 9;

        engineB[0] = 192;
        engineB[1] = 192;
		engineB[2] = 384;
		engineB[3] = 384;
		engineB[4] = 768;
		engineB[5] = 768;
		engineB[6] = 576;
		engineB[7] = 576;

		/*engine[0] = 1;
		engine[1] = 1;
		engine[2] = 2;
		engine[3] = 2;
		engine[4] = 4;
		engine[5] = 4;
		engine[6] = 8;
		engine[7] = 8;

                engineB[0] = 64;
                engineB[1] = 64;
		engineB[2] = 128;
		engineB[3] = 128;
		engineB[4] = 256;
		engineB[5] = 256;
		engineB[6] = 512;
		engineB[7] = 512;*/

		TRISBbits.TRISB0 = 0;   //output
		TRISBbits.TRISB1 = 0;   //output
		TRISBbits.TRISB2 = 0;   //output
		TRISBbits.TRISB3 = 0;   //output

		TRISCbits.TRISC6 = 0;   //output
		TRISCbits.TRISC7 = 0;   //output
		TRISCbits.TRISC8 = 0;   //output
		TRISCbits.TRISC9 = 0;   //output
		i=8;
		while (i>0)
		{
			u8 t;
                        u32 tB;

                        t = engine[i-1];
                        LATBCLR = 15;
                        LATBSET = t;

                        tB = engineB[i-1];
                        LATCCLR = 960;
                        LATCSET = tB;

                        wait(700);
			i--;
		}
		tour --;
	}
	LATBCLR = 15;
    LATCCLR = 960;
 }

void marcheAvant(u32 tour)
{
	while (tour)
	{
		u32 engine[8];
        u32 engineB[8];
        u8 i;

		engine[0] = 3;
		engine[1] = 3;
		engine[2] = 6;
		engine[3] = 6;
		engine[4] = 12;
		engine[5] = 12;
		engine[6] = 9;
		engine[7] = 9;

        engineB[7] = 192;
        engineB[6] = 192;
		engineB[5] = 384;
		engineB[4] = 384;
		engineB[3] = 768;
		engineB[2] = 768;
		engineB[1] = 576;
		engineB[0] = 576;


		TRISBbits.TRISB0 = 0;   //output
		TRISBbits.TRISB1 = 0;   //output
		TRISBbits.TRISB2 = 0;   //output
		TRISBbits.TRISB3 = 0;   //output

		TRISCbits.TRISC6 = 0;   //output
		TRISCbits.TRISC7 = 0;   //output
		TRISCbits.TRISC8 = 0;   //output
		TRISCbits.TRISC9 = 0;   //output

		i=8;
		while (i>0)
		{
			u32 t;
                        u32 tB;

                        t = engine[i-1];
                        LATBCLR = 15;
                        LATBSET = t;

                        tB = engineB[i-1];
                        LATCCLR = 960;
                        LATCSET = tB;

                        wait(700);
			i--;
		}
		tour --;
	}
	LATBCLR = 15;
        LATCCLR = 960;
 }

void marcheGauche(u32 tour)
{
	while (tour)
	{
		u32 engine[8];
        u32 engineB[8];
        u8 i;

		engine[7] = 3;
		engine[6] = 3;
		engine[5] = 6;
		engine[4] = 6;
		engine[3] = 12;
		engine[2] = 12;
		engine[1] = 9;
		engine[0] = 9;

        engineB[7] = 192;
        engineB[6] = 192;
		engineB[5] = 384;
		engineB[4] = 384;
		engineB[3] = 768;
		engineB[2] = 768;
		engineB[1] = 576;
		engineB[0] = 576;

		/*engine[0] = 1;
		engine[1] = 1;
		engine[2] = 2;
		engine[3] = 2;
		engine[4] = 4;
		engine[5] = 4;
		engine[6] = 8;
		engine[7] = 8;

                engineB[0] = 64;
                engineB[1] = 64;
		engineB[2] = 128;
		engineB[3] = 128;
		engineB[4] = 256;
		engineB[5] = 256;
		engineB[6] = 512;
		engineB[7] = 512;*/

		TRISBbits.TRISB0 = 0;   //output
		TRISBbits.TRISB1 = 0;   //output
		TRISBbits.TRISB2 = 0;   //output
		TRISBbits.TRISB3 = 0;   //output

		TRISCbits.TRISC6 = 0;   //output
		TRISCbits.TRISC7 = 0;   //output
		TRISCbits.TRISC8 = 0;   //output
		TRISCbits.TRISC9 = 0;   //output
		i=8;
		while (i>0)
		{
			u8 t;
                        u32 tB;

                        t = engine[i-1];
                        LATBCLR = 15;
                        LATBSET = t;

                        tB = engineB[i-1];
                        LATCCLR = 960;
                        LATCSET = tB;

                        wait(700);
			i--;
		}
		tour --;
	}
	LATBCLR = 15;
    LATCCLR = 960;
 }

void run_motor(u8 ins)
{
	if (ins == I_UP)
		marcheAvant(255);
	else if (ins == I_LFT)
		marcheGauche(250);
	else if (ins == I_RGT)
		marcheDroite(250);
}