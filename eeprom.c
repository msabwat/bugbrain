
#include "bugbrain.h"

u8 write2SPI(u8 i)
{
	SPI2BUF = i;
	while (!SPI2STATbits.SPIRBF);
	return SPI2BUF;
}

void write_enable(void)
{
	LATAbits.LATA3 = 0;
	write2SPI(6);
	LATAbits.LATA3 = 1;
}

u8 read_status(u8 dummy)
{
	LATAbits.LATA3 = 0;
	write2SPI(5);
	u8 i = write2SPI(dummy);
	LATAbits.LATA3 = 1;
	return i;
}

void write_eeprom(u8 data, u32 address)
{
	while (read_status(0) & 0x1);
	write_enable();
	LATAbits.LATA3 = 0;

	write2SPI(2);

	write2SPI(address >> 16);
	write2SPI(address >> 8);
	write2SPI(address);

	write2SPI(data);
	LATAbits.LATA3 = 1;
}

u8 read_eeprom(u32 address)
{
	while (read_status(0) & 0x1);
	LATAbits.LATA3 = 0;

	write2SPI(3);

	write2SPI(address >> 16);
	write2SPI(address >> 8);
	write2SPI(address);

	u8 i = write2SPI(0);
	LATAbits.LATA3 = 1;

	return i;
}