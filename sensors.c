
#include "bugbrain.h"

u8 check_color(u8 color)
{
	u32 rgb_buf;
	u32 diff_red;
	u32 diff_blue;
	u32 diff_green;
	u32 val;

	if (color == ' ')
		return 1;

	// sampling red
	RED_SENS = 1;
	AD1CON1bits.ON = 1;
	rgb_buf = readAN(1);
	RED_SENS = 0;
	AD1CON1bits.ON = 0;
	if (rgb_buf >= RED_REF)
		diff_red = rgb_buf - RED_REF;
	else if (RED_REF >= rgb_buf)
		diff_red = RED_REF - rgb_buf;

	//sampling green
	GREEN_SENS = 1;
	AD1CON1bits.ON = 1;
	rgb_buf = readAN(1);
	GREEN_SENS = 0;
	AD1CON1bits.ON = 0;
	if (rgb_buf >= GREEN_REF)
		diff_green = rgb_buf - GREEN_REF;
	else if (GREEN_REF >= rgb_buf)
		diff_green = GREEN_REF - rgb_buf;

	//screening blue
	BLUE_SENS = 1;
	AD1CON1bits.ON = 1;
	rgb_buf = readAN(1);
	BLUE_SENS = 0;
	AD1CON1bits.ON = 0;
	if (rgb_buf >= BLUE_REF)
		diff_blue = rgb_buf - BLUE_REF;
	else if (BLUE_REF >= rgb_buf)
		diff_blue = BLUE_REF - rgb_buf;

	val = MIN((MIN(diff_blue,diff_green)),diff_red);
	// Testing
//	if (val == diff_blue)
//		return 'B';
//	else if (val == diff_red)
//		return 'R';
//	else if (val == diff_green)
//		return 'G';
	if ((val == diff_blue) && (color == 'B'))
		return 1;
        else if ((val == diff_red) && (color == 'R'))
		return 1;
        else if ((val == diff_green) && (color == 'G'))
		return 1;
        if (val < 50)
            //black
            return 0;
	return 0;
}