
#include "bugbrain.h"

void print_charf1(u8 pos)
{
	send_data(0,0,0b01000000);

	send_data(1,0,0b00011110);
	send_data(1,0,0b00010000);
	send_data(1,0,0b00011001);
	send_data(1,0,0b00010011);
	send_data(1,0,0b00010001);
	send_data(1,0,0b00000001);
	send_data(1,0,0b00000000);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000000);
}

void print_charf2(u8 pos)
{
	send_data(0,0,0b01001000);

	send_data(1,0,0b00011110);
	send_data(1,0,0b00010000);
	send_data(1,0,0b00011011);
	send_data(1,0,0b00010001);
	send_data(1,0,0b00010011);
	send_data(1,0,0b00000010);
	send_data(1,0,0b00000011);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000001);
}

void print_charf3(u8 pos)
{
	send_data(0,0,0b01010000);

	send_data(1,0,0b00011110);
	send_data(1,0,0b00010000);
	send_data(1,0,0b00011011);
	send_data(1,0,0b00010001);
	send_data(1,0,0b00010011);
	send_data(1,0,0b00000001);
	send_data(1,0,0b00000011);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000010);
}

void print_charup(u8 pos)
{
	send_data(0,0,0b01011000);

	send_data(1,0,0b00000100);
	send_data(1,0,0b00001110);
	send_data(1,0,0b00011111);
	send_data(1,0,0b00001110);
	send_data(1,0,0b00001110);
	send_data(1,0,0b00001110);
	send_data(1,0,0b00001110);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000011);
}

void print_charlft(u8 pos)
{
	send_data(0,0,0b01100000);

	send_data(1,0,0b00000100);
	send_data(1,0,0b00001100);
	send_data(1,0,0b00011111);
	send_data(1,0,0b00001101);
	send_data(1,0,0b00000101);
	send_data(1,0,0b00000001);
	send_data(1,0,0b00000001);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000100);
}

void print_charrgt(u8 pos)
{
	send_data(0,0,0b01101000);

	send_data(1,0,0b00000100);
	send_data(1,0,0b00000110);
	send_data(1,0,0b00011111);
	send_data(1,0,0b00010110);
	send_data(1,0,0b00010100);
	send_data(1,0,0b00010000);
	send_data(1,0,0b00010000);

	send_data(0,0,0b10000000 + pos);
	send_data(1,0,0b00000101);
}

void print_main(void)
{
	clear();
	putlcd("Main instruct. set: ");
	move_right(52);
	u8 j = 0;
	for (j = 0; j <20;j++)
	{
		send_data(1,0, '#');
	}
}

void print_f1(void)
{

	clear();
	putlcd("F1 instruct. set:   ");
	move_right(52);
	u8 j = 0;
	for (j = 0; j <20;j++)
	{
		send_data(1,0, '#');
	}
}

void print_f2(void)
{

	clear();
	putlcd("F2 instruct. set:   ");
	move_right(52);
	u8 j = 0;
	for (j = 0; j <20;j++)
	{
		send_data(1,0, '#');
	}
}

void print_f3(void)
{

	clear();
	putlcd("F3 instruct. set:   ");
	move_right(52);
	u8 j = 0;
	for (j = 0; j <20;j++)
	{
		send_data(1,0, '#');
	}
}