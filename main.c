/* 
 * File:   main.c
 * Author: Bug Brain team
 *
 * Created on August 20, 2018, 12:13 AM
 */

#include "confbits.h"
#include "bugbrain.h"

u8 cnt;
u32 key_buf = 0;
u8 button_state = 1;
u8 button;
u8 i;
//Interface variables
u8 mode = 0;
u8 cursor_ins = 0;
u8 cursor_col = 0;

/* MODES*/
// Mode 1: Instruction Set (Main, F1, F2, F3)
// Mode 2: Save

// ins possible values : F1,F2,F3 [30,40,50] | LFT,UP,RGT [60,80,100]
// col ['R','G','B',' ']

s_insSet current;
s_insSet saved;

void init_inst_set(void)
{
	u8 i;

	// read from eeprom (saved)
	for (i = 0; i < 20; i++)
	{
		current.main[i] = (s_cmd){' ', ' '};
		current.f1[i] = (s_cmd){' ', ' '};
		current.f2[i] = (s_cmd){' ', ' '};
		current.f3[i] = (s_cmd){' ', ' '};
	}
}

u8 run_ins_set(s_cmd *ins_set)
{
	u8 i;

	for (i = 0; i < 20; i++)
	{
                if (i == 0)
                    clear();
                if (ins_set[i].ins == I_UP)
                    print_charup(i);
                else if (ins_set[i].ins == I_LFT)
                    print_charlft(i);
                else if (ins_set[i].ins == I_RGT)
                    print_charrgt(i);
                cnt++;
		if (cnt == 200)
			break;
                if (ins_set[i].color != ' ')
                {
                    if (check_color(ins_set[i].color) == 0)
                    {
                         clear();
                         putlcd("No ! Try Again !");
                         return 0;
                    }
                }
		if (ins_set[i].ins == ' ')
		{
                        clear();
                        putlcd("C'est fini !");
			return 0;
		}
		if ((ins_set[i].ins == I_UP)
				|| (ins_set[i].ins == I_LFT) || (ins_set[i].ins == I_RGT))
			run_motor(ins_set[i].ins);
//		if (ins_set[i].ins == I_F1)
//			run_ins_set((s_cmd *)current.f1);
//		if (ins_set[i].ins == I_F2)
//			run_ins_set((s_cmd *)current.f2);
//		if (ins_set[i].ins == I_F3)
//			run_ins_set((s_cmd *)current.f3);
	}
	return 1;
}



//void handler(u32 key_buf)
//{
//	handle_key(key_buf);
//	current.f1[0] = (s_cmd){'a','R'};
//	// handle save, and run separately
//}



int main(void)
{
    init();
    lcd_init();
    initAdc();
    initSPI();
    init_inst_set();

    print_main();
    while (42)
	{
                AD1CON1bits.ON = 1;
		key_buf = readAN0();
		AD1CON1bits.ON = 0;
                if (cursor_ins >= 18 || cursor_col >= 18)
                {
                    cursor_ins = 0;
                    cursor_col = 0;
                }
		if (key_buf == LFT)
		{
			print_charlft(20 + cursor_ins);
			current.main[cursor_ins].ins = I_LFT;
                        move_right(43);
                        if ((current.main[cursor_ins - 1].color == ' ') && (cursor_ins != 0))
                            cursor_col++;
                        cursor_ins++;

			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == UP)
		{
			print_charup(20 + cursor_ins);
                        current.main[cursor_ins].ins = I_UP;
                        move_right(42);
			 if ((current.main[cursor_ins - 1].color == ' ') && (cursor_ins != 0))
                            cursor_col++;
                        cursor_ins++;

			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == RGT)
		{
			print_charrgt(20 + cursor_ins);
                        current.main[cursor_ins].ins = I_RGT;
                        move_right(44);
			if ((current.main[cursor_ins - 1].color == ' ') && (cursor_ins != 0))
                            cursor_col++;
                        cursor_ins++;

	        while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0(0);
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == MODE)
		{
			//Not implemented
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if ((key_buf == RED) && (cursor_ins != 0))
		{
                        putlcd("R");
                        current.main[cursor_col].color = 'R';
                        cursor_col++;
                        while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if ((key_buf == GREEN) && (cursor_ins != 0))
		{
			putlcd("G");
                        current.main[cursor_col].color = 'G';
                        cursor_col++;
                        while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if ((key_buf == BLUE) && (cursor_ins != 0))
		{
			putlcd("B");
                        current.main[cursor_col].color = 'B';
                        cursor_col++;
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == SAVE)
		{
                        //Not Implemented
			//Send instruction set to eeprom
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == F1)
		{
			//not implemented
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == F2)
		{
			//not implemented
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == F3)
		{
                    // Not implemented
//			cursor = 0;
//			print_f3();
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == DEL)
		{
//                    current.main[cursor_ins].ins = ' ';
//                    cursor_ins--;
//                    current.main[cursor_col].color = ' ';
//                    cursor_col--;
		    while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == MV_LFT)
		{
			//Not Implemented
			//send_data(0,0,0b00010000);
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
	}
		else if (key_buf == RUN)
		{
			run_ins_set(current.main);
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == MV_RGT)
		{
			//Not Implemented
			//send_data(0,0,0b00010100);
			while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
		else if (key_buf == RESET)
		{
                        print_main();
                        cursor_ins = 0;
                        cursor_col = 0;
                        while (key_buf != 0)
			{
				AD1CON1bits.ON = 1;
				key_buf = readAN0();
				AD1CON1bits.ON = 0;
			}
		}
	}
	return (0);
}