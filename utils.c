
#include "bugbrain.h"

u32 readAN(u8 channel)
{
	AD1CHSbits.CH0SA = channel;
	
	AD1CON1bits.SAMP = 1;
	wait2(500);
	AD1CON1bits.SAMP = 0;

    while (!AD1CON1bits.DONE);
	return ADC1BUF0;
}

u32 readAN0(void)
{
	AD1CHSbits.CH0SA = 0;

	AD1CON1bits.SAMP = 1;
	wait2(1);
	AD1CON1bits.SAMP = 0;

    while (!AD1CON1bits.DONE);
	return ADC1BUF0;
}

//
//u8 *ft_itoa(u32 adcbuf, u8 *buf)
//{
//	buf[5] = '\0';
//	u8 i = 4;
//	while (adcbuf)
//	{
//		buf[i] = '0' + (adcbuf % 10);
//		adcbuf /= 10;
//		i--;
//	}
//	return buf;
//}